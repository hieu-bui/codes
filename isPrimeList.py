import math

def answer(n):
     odds = range(3,n+1,2)
     sieve = set(sum([range(q*q, n+1, q+q) for q in odds],[]))
     master =[2] +  [p for p in odds if p not in sieve]
     print (master)

     primeString = ""
     for i in master:
         primeString += str(i)

     print ("Prime#: " + str(len(master)), "PrimeString: " + str(len(primeString))+primeString)
answer(21000)

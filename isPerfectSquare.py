import math
def answer(n):
    perfectSquare = []
    myString = ""
    for i in range(1,n,2):

        if int(math.sqrt(i)) * int(math.sqrt(i)) == int(i):
            perfectSquare.append(i)
            myString += str(i)

    print("Perfect Square", perfectSquare)
    #print(myString)

# boolean function to return if n is a perfect square
    def isPS(n):
        return int(math.sqrt(n)) * int (math.sqrt(n)) == int(n)


    odds = range(3,n+1000,2)
    sieve = set(sum([range(q*q, n+1000, q+q) for q in odds],[]))
    master =[2] +  [p for p in odds if p not in sieve]

    primeString = ""
    for i in master:
        primeString +=str(i)

    myID = primeString[n:n+5]
    print ("Prime String", primeString)
    print ("re_ID", myID)

answer(1000)

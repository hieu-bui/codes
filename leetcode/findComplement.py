class Solution(object):
    def findComplement(self, num):
        """
        :type num: int
        :rtype: int
        """
        #foo = '{0:031b}'.format(num)
        foo = bin(num)[2:]
        #print (type(foo))
        bar = ''
        for i in range(0,len(foo)):
            if foo[i] == '1': #compare str need to use ==
                bar += '0'
            else:
                bar += '1'
        return int(bar,2)

    print(findComplement(0,10))

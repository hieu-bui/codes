class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        #if len(nums) == 1 and nums[0] == 1:
        #    return nums[0]
        #else:

        count = 0
        ans = []
        for i in nums:
            if i == 1:
                count += 1
            else:
                ans.append(count)
                count = 0

        ans.append(count)

        ans.sort(reverse=True)
            #print ans
        return ans[0]

    print findMaxConsecutiveOnes(0,[1,1,0,1,1,1])

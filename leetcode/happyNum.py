
class Solution(object):
    def isHappy(self,n):
        numSet = set()
        while n != 1 and n not in numSet:
            numSet.add(n)
            sum = 0
            while n:
                digit = n % 10
                sum += digit*digit
                n /= 10   "divide and assign"
            n = sum
        return n == 1

a = Solution()
print(a.isHappy(19))
print(a.isHappy(03))

class Solution(object):
    def hammingDistance(self, x, y):
        """
        :type x: int
        :type y: int
        :rtype: int
        """
        foo = '{0:031b}'.format(x)
        bar = '{0:031b}'.format(y)

        count = 0
        for i in range(0,len(bar)):
            if bar[i] != foo[i]:
                count = count + 1
        return count

    print (hammingDistance(0,1,4))

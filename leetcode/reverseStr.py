class Solution(object):
    def reverseString(self,s):
        n = len(s)
        st = list(s)
        if n%2 == 1:
            foo = (n-1)/2
        else:
            foo = n/2
        for i in range(0,foo):
            bar = st[n-1-i]
            st[n-1-i] = st[i]
            st[i] = bar
        return "".join(st)

a = Solution()
print(a.reverseString('hello'))
print(a.reverseString('AAATTTGGGGCCCCCTTTT'))

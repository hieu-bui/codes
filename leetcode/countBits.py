class Solution(object):
    def countBits(self, num):
        """
        :type num: int
        :rtype: List[int]
        """
        ans = [0]
        for x in range(1, num + 1):
            ans += ans[x >> 1] + (x & 1),
        return ans

a = Solution()
# x >> 1 : x/2^1 for example 20 >> 1 : 20/2^1 = 10
# x & 1 : bitwise AND
# T = 1, (a tuple), b ,= T (an integer)
# , : make a tuple
print(a.countBits(4))


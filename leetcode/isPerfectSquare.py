class Solution(object):
    def isPerfectSquare(self, num):
        """
        :type num: int
        :rtype: bool
        """
        L, R = 1, (num >> 1) + 1
        while L <= R:
            m = L + ((R - L) >> 1)
            mul = m ** 2
            if mul == num:
                return True
            elif mul > num:
                R = m - 1
            else:
                L = m + 1
        return False

foo = Solution()
print(foo.isPerfectSquare(16))

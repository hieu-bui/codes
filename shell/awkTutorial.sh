echo 'one two three four' | awk '{print $1}'
#one
echo 'one two three four' | awk '{print $3}'
#three
echo 'one two three four' | awk '{print $3,$1}' 
#three one
echo 'one two three four' | awk '{print "foo:",$3,"| bar:",$1}' 
#foo: three | bar: one
echo 'one mississippi,two mississippi,three mississippi,four mississippi' | awk -F , '{print $4}' 
#four mississippi
echo 'one two three four' | awk '{print $NF}' 
#four
echo 'one two three four' | awk '{print $(NF-1)}' 
#three
echo 'one two three four' | awk '{print $((NF/2)+1)}' 
#three

echo -e 'one 1\ntwo 2' | awk '{print $2}' 
#1
#2

echo -e 'one 1\ntwo 2' | awk '{sum+=$2} END {print sum}' 
#3

#cat requests.log 

#< requests.log awk '{print $NF}' 

# < requests.log awk '{totalBytes+=$NF} END {print totalBytes}' 

def answer(n,b):
    k = len(n) #int
    this = '6174'
    #this = '210111'
    if n == this:
        print(n)
    else:
        print(n,b)
        x = []
        y = []
        for ch in n:
            x.append(int(ch))
            y.append(int(ch))
        x.sort(reverse=True)
        y.sort(reverse=False)

        sx = ''
        sy = ''
        for ch in x:
            sx += str(ch)
        for ch in y:
            sy += str(ch)
        z = int(sx,b) - int(sy,b)

        zp=''
        if len(str(z)) != k:
            diff = k-len(str(z))  #determine 0s
            zp = '0'*diff + str(z)  #padding 0s
           #print(zp,b)          #determine next id and base
            n = zp
            answer(n,b)
        else:
            n = str(z)
            answer(n,b)
    # need to write a function to test for cycle
    # need to return an integer to indicate the occurance of cycle

answer('2111',10)
#answer('210022',3)
